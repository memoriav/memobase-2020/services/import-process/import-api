FROM index.docker.io/library/python:3.8

ARG K8S_VERSION=v1.25.6
ARG HELM_VERSION=v3.12.0

ENV FLASK_APP import_api_app

EXPOSE 5000
ENTRYPOINT ["gunicorn"]
CMD [ "wsgi:app", "--config", "/configs/gunicorn.conf.py"]

RUN cd /usr/bin \
  && wget -q https://storage.googleapis.com/kubernetes-release/release/${K8S_VERSION}/bin/linux/amd64/kubectl \
  && chmod +x ./kubectl \
  && wget -q https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz \
  && tar -xvzf helm-${HELM_VERSION}-linux-amd64.tar.gz \
  && mv linux-amd64/helm helm \
  && rm -rf linux-amd64

WORKDIR /
ADD setup.py /

RUN mkdir import_api_app \
  && pip install --no-cache-dir -e .

WORKDIR /import_api_app

ADD import_api_app .
