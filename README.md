### Import Process API Service
This service serves as a bridge between Drupal and the backend services of 
Memobase.

Check the /apidocs endpoint for the Swagger generated documentation.

[Confluence Doku](https://ub-basel.atlassian.net/wiki/spaces/MEMOBASE/pages/20316175/Import+Prozess+API)

#### Configuration

- `KAFKA_BOOTSTRAP_SERVERS`: Kafka bootstrap server string used by the service.
- `KAFKA_SECURITY_PROTOCOL`: Protocol used to communicate with brokers. Valid values are: `PLAINTEXT`, `SSL`, `SASL_PLAINTEXT`, `SASL_SSL`
- `KAFKA_SSL_CA_LOCATION`: Path to trusted certificates in PEM format with X.509 certificates
- `KAFKA_SSL_CERTIFICATE_LOCATION`: Path to client certificate in PEM format.
- `KAFKA_SSL_KEY_LOCATION`: Path to private key in PEM format with PKCS#8 keys. If the key is encrypted, key password must be specified using `KAFKA_SSL_KEY_PASSWORD`
- `DRUPAL_USERNAME`: Username for the basic auth of Drupal.
- `DRUPAL_PASSWORD`: Password for the basic auth of Drupal.
- `DRUPAL_API_KEY`: Used to access the Drupal JSON API.
- `IMPORT_API_URL`: The base URL of this service.
- `DRUPAL_API_URL`: The base URL of the drupal instance this service should communicate with.
- `INGEST_API_URL`: The base URL of the ingest service for record sets and institutions.
- `TOPIC_CONFIGS`: The topic where this service sends the pipeline configurations to.
- `TOPIC_DRUPAL_EXPORT_RECORD_SET`: This topic defines where the service sends the record sets to.
- `TOPIC_DRUPAL_EXPORT_INSTITUTION`: This topic defines where the service sends the institutions to.
- `CLEAR_CACHE_URL`: Drupal Endpoint URL to clear the local cache.
- `LOG_LEVEL`: Application log level: DEBUG, INFO, WARNING or ERROR
- Text File Validation Service Configs:
  - `TFV_KAFKA_SERVER_CONFIGS`: The kafka bootstrap server configmap name.
  - `TFV_SFTP_CONFIGS`: The configmap name of the sftp server configs.
  - `TFV_TOPIC_NAME`: Topic name where the messages are sent to.
  - `TFV_REPORTING_TOPIC_NAME`: Topic name where the reports are sent to.
- sFTP Settings
  - `SFTP_HOST`, `SFTP_PORT`, `SFTP_USER`, `SFTP_PASSWORD`
