# Import API Service
# Copyright (C) 2020-2021 Memobase Project
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import unittest
import os

import requests
from requests.auth import HTTPBasicAuth


class TestBasicAuth(unittest.TestCase):
    def test_get_institution(self):
        institution_id = "901b3831-3700-4974-a358-ef3e39dc6438"
        url = f"{os.environ['DRUPAL_API_URL']}/de/jsonapi/node/institution/{institution_id}"
        headers = {"X-API-Key": os.environ["DRUPAL_API_KEY"]}
        user = os.environ["DRUPAL_USERNAME"]
        password = os.environ["DRUPAL_PASSWORD"]

        response = requests.get(
            url, headers=headers, auth=HTTPBasicAuth(user, password)
        )

        assert response.ok
