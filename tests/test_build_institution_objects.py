from import_api_app.helpers.institution_service import InstitutionObjectBuilder
import json
import unittest


class TestInstitutionObjectBuild(unittest.TestCase):
    def test_build_institution(self):
        path = "tests/institution_objects"
        with open(f"{path}/addresses.json") as f:
            addresses = InstitutionObjectBuilder._build_institution_addresses_object(
                json.load(f)
            )
        with open(f"{path}/institution_types.json") as f:
            institution_types = (
                InstitutionObjectBuilder._build_institution_types_object(json.load(f))
            )
        with open(f"{path}/record_sets.json") as f:
            record_sets_ids = InstitutionObjectBuilder._build_record_set_ids_object(
                json.load(f)
            )
        with open(f"{path}/collections.json") as f:
            collections = InstitutionObjectBuilder._build_collections_object(
                json.load(f)
            )
        with open(f"{path}/institution_de.json") as f:
            institution_de = json.load(f)
        with open(f"{path}/institution_fr.json") as f:
            institution_fr = json.load(f)
        with open(f"{path}/institution_it.json") as f:
            institution_it = json.load(f)
        institution = {"de": institution_de, "fr": institution_fr, "it": institution_it}
        computed_result = InstitutionObjectBuilder._build_institution_object(
            institution, addresses, institution_types, record_sets_ids
        )
        computed_result["collections"] = collections
        with open(f"{path}/result.json") as f:
            actual_result = json.load(f)
        print(json.dumps(computed_result))
        assert deep_compare(actual_result, computed_result)


def deep_compare(left: dict, right: dict) -> bool:
    keys_left = sorted(left.keys())
    keys_right = sorted(right.keys())
    if keys_left != keys_right:
        print(f"{keys_left} not equal {keys_right}")
        return False
    for k in keys_left:
        if type(left[k]) != type(right[k]):
            print(f"type of {keys_left} not equal type of {keys_right}")
            return False
        elif type(left[k]) == dict:
            if not deep_compare(left[k], right[k]):
                return False
        elif type(left[k]) == list:
            if not _deep_compare_list(left[k], right[k]):
                return False
        else:
            if left[k] != right[k]:
                print(f"{left[k]} not equal {right[k]}")
                return False
    return True


def _deep_compare_list(left: list, right: list) -> bool:
    if len(left) != len(right):
        print("List are not of equal length")
        return False
    sorted_left = sorted(left)
    sorted_right = sorted(right)
    for i in range(len(sorted_left)):
        if type(sorted_left[i]) != type(sorted_right[i]):
            print(f"type of {sorted_left[i]} not equal type of {sorted_right[i]}")
            return False
        elif type(sorted_left[i]) == dict:
            if not deep_compare(sorted_left[i], sorted_right[i]):
                return False
        elif type(sorted_left[i]) == list:
            if not _deep_compare_list(sorted_left[i], sorted_right[i]):
                return False
        else:
            if sorted_left[i] != sorted_right[i]:
                print(f"{sorted_left[i]} not equal {sorted_right[i]}")
                return False
    return True
