# Import API Service
# Copyright (C) 2020-2021 Memobase Project
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import logging
import os
import subprocess

from flasgger import Swagger
from flask import send_from_directory, redirect

from import_api_app.app import app
from import_api_app.helpers.error import ImportApiError
from import_api_app.resources.clear_cache import ClearCache
from import_api_app.resources.delete_institution import DeleteInstitution
from import_api_app.resources.delete_record import DeleteRecord
from import_api_app.resources.delete_recordset import DeleteRecordset
from import_api_app.resources.delete_object_v2 import DeleteObjectV2
from import_api_app.resources.fetch_mapping_file import FetchMappingFile
from import_api_app.resources.import_process_start import ImportProcessStart
from import_api_app.resources.srg_api_import_start import SrgApiImportStart
from import_api_app.resources.oai_import_start import OaiImportStart
from import_api_app.resources.update_institution import (
    UpdateInstitution,
    UpdateInventoryCollection,
)
from import_api_app.resources.update_record_set import UpdateRecordSet
from import_api_app.resources.write_documents_reports import WriteJobResultToDrupal
from import_api_app.resources.write_groups_reports import WriteTypeReportToDrupal


def _build_oci_uri(root, chart_name):
    if root.endswith("/"):
        return root + chart_name
    else:
        return f"{root}/{chart_name}"


# If app is started via gunicorn
if __name__ != "__main__":
    gunicorn_logger = logging.getLogger("gunicorn.error")
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)
    app.logger.info("Starting production server")

try:
    # the rstrips are there when a secret contains a newline at the end. This
    # problem pops up from time to time when a secret is created from the console.
    app.config["import-api-url"] = os.environ["IMPORT_API_URL"]
    app.config["kafka-broker-url"] = os.environ["KAFKA_BOOTSTRAP_SERVERS"]
    app.config["drupal-user"] = os.environ["DRUPAL_USERNAME"].rstrip()
    app.config["drupal-password"] = os.environ["DRUPAL_PASSWORD"].rstrip()
    app.config["drupal-api-url"] = os.environ["DRUPAL_API_URL"].rstrip()
    app.config["drupal-api-key"] = os.environ["DRUPAL_API_KEY"].rstrip()
    app.config["ingest-api-url"] = os.environ["INGEST_API_URL"].rstrip()
    app.config["clear-cache-url"] = os.environ["CLEAR_CACHE_URL"]
    app.config["topic-configs"] = os.environ["TOPIC_CONFIGS"]
    app.config["topic-drupal-export-record-set"] = os.environ[
        "TOPIC_DRUPAL_EXPORT_RECORD_SET"
    ]
    app.config["topic-drupal-export-institution"] = os.environ[
        "TOPIC_DRUPAL_EXPORT_INSTITUTION"
    ]
    app.config["sftp_host"] = os.environ["SFTP_HOST"].rstrip()
    app.config["sftp_port"] = os.environ["SFTP_PORT"].rstrip()
    app.config["sftp_user"] = os.environ["SFTP_USER"].rstrip()
    app.config["sftp_password"] = os.environ["SFTP_PASSWORD"].rstrip()
    app.config["sftp_base_path"] = os.environ["SFTP_BASE_PATH"]
    app.config["srg_crawler_k8s_name"] = os.environ["SRG_CRAWLER_K8S_NAME"]
    app.config["oai_client_k8s_name"] = os.environ["OAI_CLIENT_K8S_NAME"]

    app.config["kafka-ssl-ca-file"] = os.environ["KAFKA_SSL_CA_CERTIFICATE"]
    app.config["kafka-ssl-cert-file"] = os.environ["KAFKA_SSL_CERTIFICATE"]
    app.config["kafka-ssl-key-file"] = os.environ["KAFKA_SSL_KEY"]

    app.config["tfv-topic-name"] = os.environ["TFV_TOPIC_NAME"]
    app.config["delete-service-chart-uri"] = _build_oci_uri(
        os.environ["CHARTS_REGISTRY_URL"], os.environ["DELETE_SERVICE_CHART_NAME"]
    )
    app.config["delete-service-version"] = os.environ["DELETE_SERVICE_VERSION"]
    app.config["NAMESPACE"] = os.environ["NAMESPACE"]
except KeyError as ex:
    raise ImportApiError(f"Environment variable {ex} is missing.")

app.config["SWAGGER"] = {
    "title": "Memobase Import API",
    "version": "dev",
    "uiversion": 3,
    "termsOfService": "http://memobase.ch/de/disclaimer",
    "description": "API to start, stop, manage import processes for "
    "memobase. Will be used in the Admin Interface (Drupal).",
    "contact": {
        "name": "UB Basel",
        "url": "https://ub.unibas.ch",
        "email": "swissbib-ub@unibas.ch",
    },
    "favicon": "/favicon.ico",
}
Swagger(app)


@app.route("/")
def home():
    return redirect("/apidocs")


@app.route("/favicon.ico")
def favicon():
    return send_from_directory(
        os.path.join(app.root_path, "assets"),
        "favicon.ico",
        mimetype="image/vnd.microsoft.icon",
    )


app.add_url_rule(
    "/v2/importprocess/<institution_id>/<record_set_id>/start",
    view_func=ImportProcessStart.as_view("start_import_process"),
)

app.add_url_rule(
    "/v2/srg-import/<record_set_id>",
    view_func=SrgApiImportStart.as_view("srg_api_import_start"),
)

app.add_url_rule(
    "/v2/oai-import/<record_set_id>",
    view_func=OaiImportStart.as_view("oai_import_start"),
)

app.add_url_rule(
    "/v2/FetchMappingFile/<recordset_id>/<session_id>",
    view_func=FetchMappingFile.as_view("load_mapping_files"),
)

app.add_url_rule(
    "/v2/drupal/<job_drupal_uuid>/<job_log_drupal_uuid>",
    view_func=WriteJobResultToDrupal.as_view("write_import_process_report"),
)

app.add_url_rule(
    "/v2/drupal/WriteElementReport",
    view_func=WriteTypeReportToDrupal.as_view("write_group_pipeline_report"),
)

app.add_url_rule(
    "/v2/drupal/institution/<institution_drupal_uuid>",
    view_func=UpdateInstitution.as_view("update_institution"),
)

app.add_url_rule(
    "/v2/drupal/inventory/<inventory_collection_drupal_uuid>",
    view_func=UpdateInventoryCollection.as_view("update_inventory_collection"),
)

app.add_url_rule(
    "/v2/drupal/recordset/<record_set_drupal_uuid>",
    view_func=UpdateRecordSet.as_view("update_record_set"),
)

app.add_url_rule(
    "/v2/drupal/delete/record/<session_id>/<dryrun>",
    view_func=DeleteRecord.as_view("delete_record"),
)

app.add_url_rule(
    "/v2/drupal/delete/recordset/<session_id>/<dryrun>",
    view_func=DeleteRecordset.as_view("delete_recordset"),
)

app.add_url_rule(
    "/v2/drupal/delete/institution/<session_id>/<dryrun>",
    view_func=DeleteInstitution.as_view("delete_institution"),
)

app.add_url_rule(
    "/v2/drupal/delete/<session_id>/<dryrun>",
    view_func=DeleteObjectV2.as_view("delete_object_v2"),
)

app.add_url_rule("/v2/drupal/clearcache", view_func=ClearCache.as_view("clearcache"))

app.logger.info("Initialized API Resources.")

app.logger.info("Log in to Helm registry")
cmd = [
    "helm",
    "registry",
    "login",
    "-u",
    os.environ["CHARTS_REGISTRY_USERNAME"],
    "-p",
    os.environ["CHARTS_REGISTRY_PASSWORD"],
    "cr.gitlab.switch.ch",
]
subprocess.run(
    cmd,
    stdout=subprocess.PIPE,
    stderr=subprocess.PIPE,
    check=True,
    text=True,
)


# If app is started with Flask
if __name__ == "__main__":
    logging.basicConfig(format="%(levelname)-8s [%(filename)s:%(lineno)d] %(message)s")
    app.logger.info("Starting development server")
    app.run(host="0.0.0.0", port=5000, debug=True)
