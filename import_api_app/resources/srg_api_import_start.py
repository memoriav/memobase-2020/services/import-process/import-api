from flasgger import swag_from
from flask import request
from flask.views import MethodView
from import_api_app.app import app
from import_api_app.helpers.configuration import ConfigurationFileHandler
from import_api_app.helpers.remote_import import start_import_client


class SrgApiImportStart(MethodView):
    def __init__(self):
        self.conversion_file_handler = ConfigurationFileHandler(
            app.config["sftp_host"],
            app.config["sftp_port"],
            app.config["sftp_user"],
            app.config["sftp_password"],
            app.config["sftp_base_path"],
            app.config["kafka-broker-url"],
            app.config["topic-configs"],
            app.config["kafka-ssl-ca-file"],
            app.config["kafka-ssl-cert-file"],
            app.config["kafka-ssl-key-file"],
            app.logger,
        )

    @swag_from("SrgApiImportStart.yml")
    def post(self, record_set_id):
        app_name = app.config["srg_crawler_k8s_name"]
        return start_import_client(
            request.json, record_set_id, app_name, self.conversion_file_handler
        )
