# Import API Service
# Copyright (C) 2020-2021 Memobase Project
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import os
import uuid
import json

import traceback
import requests
from flask.views import MethodView
from kafka import KafkaProducer
from kafka.errors import NoBrokersAvailable


from import_api_app.helpers.institution_service import (
    ResultException,
    DrupalException,
    InstitutionObjectBuilder,
)
from import_api_app.app import app


def connect_producer():
    return KafkaProducer(
        bootstrap_servers=app.config["kafka-broker-url"],
        value_serializer=lambda m: json.dumps(m, ensure_ascii=False).encode("utf-8"),
        security_protocol=app.config["kafka-security-protocol"],
        ssl_check_hostname=True,
        ssl_cafile=app.config["kafka-ssl-ca-location"],
        ssl_certfile=app.config["kafka-ssl-certificate-location"],
        ssl_keyfile=app.config["kafka-ssl-key-location"],
    )


class UpdateInventoryCollection(MethodView):
    def __init__(self):
        try:
            self.producer = connect_producer()
        except NoBrokersAvailable as err:
            self.producer = None
            app.logger.error(str(err))

    def get(self, inventory_collection_drupal_uuid):
        """
        Update the institution with the given "pseudo collection" drupal UUID in the backend.
        ---
        tags:
          - Update Institution via Pseudo Collection UUID
        parameters:
          - in: path
            name: inventory_collection_drupal_uuid
            required: true
            description: The UUID of the updated inventory collection
            example: 0c4c777c-94f8-45ba-945a-bfe6967d40da
            type: string
        responses:
          200:
            description: Success, the information has been written into
                the kafka topic
            schema:
              properties:
                status:
                  type: string
                  example: SUCCESS/FAILURE
                  enum: ['SUCCESS', 'FAILURE']
                topic_value:
                  type: string/json
                  example: the value written into the topic

        """
        app.logger.debug(
            f"Request for updating pseudo collection with UUID {inventory_collection_drupal_uuid}"
        )
        try:
            url = (
                os.environ["INGEST_API_URL"]
                + f"/api/v1/inventorycollection/{inventory_collection_drupal_uuid}"
            )
            app.logger.info(f"Sending request to new ingest service: {url}")
            response = requests.put(url)
            app.logger.info(f"Response: {response.text}")
        except Exception as ex:
            app.logger.error(f"Exception with ingest service: {ex}")
        institution_service = InstitutionObjectBuilder(app.config)
        try:
            result = institution_service.get_by_collections_uuid(
                inventory_collection_drupal_uuid
            )
            producer_topic = app.config["topic-drupal-export-institution"]
            headers = [
                ("recordSetId", bytes("none", encoding="utf-8")),
                ("sessionId", bytes(str(uuid.uuid4()), encoding="utf-8")),
                (
                    "institutionId",
                    bytes(result.get("field_memobase_id") or "", encoding="utf-8"),
                ),
                ("isPublished", bytes(str(result["status"]), encoding="utf-8")),
            ]
            key = bytes(result.get("field_memobase_id") or "", encoding="utf-8")
            app.logger.debug("Producer Topic: " + producer_topic)
            while self.producer is None:
                self.producer = connect_producer()
            self.producer.send(producer_topic, result, key, headers=headers)
        except DrupalException as ex:
            app.logger.error(str(ex))
            return {"status": "FAILURE", "message": str(ex)}, 500
        except ResultException as ex:
            app.logger.error(str(ex))
            return {"status": "FAILURE", "message": str(ex)}, 500
        except LookupError as ex:
            msg = f"Could not find key ({inventory_collection_drupal_uuid}): {ex}."
            app.logger.error(msg)
            return {"status": "FAILURE", "message": str(ex)}, 500
        except Exception as ex:
            msg = f"Unknown Exception ({inventory_collection_drupal_uuid}): {ex}"
            app.logger.error(msg)
            return {"status": "FAILURE", "message": str(ex)}, 500
        app.logger.debug("success for " + inventory_collection_drupal_uuid)
        return {
            "status": "SUCCESS",
            "topic_key": result.get("field_memobase_id"),
            "topic_value": result,
        }, 200


class UpdateInstitution(MethodView):
    def __init__(self):
        try:
            self.producer = connect_producer()
        except NoBrokersAvailable as err:
            self.producer = None
            app.logger.error(str(err))

    def get(self, institution_drupal_uuid):
        """
        Update the institution with the given drupal UUID in the backend.
        ---
        tags:
          - Update Institution via Institution UUID
        parameters:
          - in: path
            name: institution_drupal_uuid
            required: true
            description: The UUID of the updated institution
            example: 0c4c777c-94f8-45ba-945a-bfe6967d40da
            type: string
        responses:
          200:
            description: Success, the information has been written into
                the kafka topic
            schema:
              properties:
                status:
                  type: string
                  example: SUCCESS/FAILURE
                  enum: ['SUCCESS', 'FAILURE']
                topic_value:
                  type: string/json
                  example: the value written into the topic

        """
        app.logger.debug(
            f"Request for updating institution with UUID {institution_drupal_uuid}"
        )
        try:
            url = (
                os.environ["INGEST_API_URL"]
                + f"/api/v1/institution/{institution_drupal_uuid}"
            )
            app.logger.info(f"Sending request to new ingest service: {url}")
            response = requests.put(url)
            app.logger.info(f"Response: {response.text}")
        except Exception as ex:
            app.logger.error(f"Exception with ingest service: {ex}")

        institution_service = InstitutionObjectBuilder(app.config)
        try:
            result = institution_service.get_by_institution_uuid(
                institution_drupal_uuid
            )
            producer_topic = app.config["topic-drupal-export-institution"]
            headers = [
                ("recordSetId", bytes("none", encoding="utf-8")),
                ("sessionId", bytes(str(uuid.uuid4()), encoding="utf-8")),
                (
                    "institutionId",
                    bytes(result.get("field_memobase_id") or "", encoding="utf-8"),
                ),
                ("isPublished", bytes(str(result["status"]), encoding="utf-8")),
            ]
            key = bytes(result.get("field_memobase_id") or "", encoding="utf-8")
            app.logger.debug("Producer Topic: " + producer_topic)
            while self.producer is None:
                self.producer = connect_producer()
            self.producer.send(producer_topic, result, key, headers=headers)
        except DrupalException as ex:
            app.logger.error(str(ex))
            app.logger.debug(traceback.format_exc())
            return {"status": "FAILURE", "message": str(ex)}, 500
        except ResultException as ex:
            app.logger.error(str(ex))
            app.logger.debug(traceback.format_exc())
            return {"status": "FAILURE", "message": str(ex)}, 500
        except LookupError as ex:
            msg = f"Could not find key ({institution_drupal_uuid}): {ex}."
            app.logger.error(msg)
            app.logger.debug(traceback.format_exc())
            return {"status": "FAILURE", "message": str(ex)}, 500
        except Exception as ex:
            msg = f"Unknown Exception ({institution_drupal_uuid}): {ex}"
            app.logger.error(msg)
            app.logger.debug(traceback.format_exc())
            return {"status": "FAILURE", "message": str(ex)}, 500
        else:
            app.logger.debug("success for " + institution_drupal_uuid)
            return {
                "status": "SUCCESS",
                "topic_key": result.get("field_memobase_id"),
                "topic_value": result,
            }, 200
