# Import API Service
# Copyright (C) 2020-2021 Memobase Project
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import hashlib
import os.path
import traceback
import uuid
from typing import Dict, Union, Tuple, List

from flasgger import swag_from
from flask import request
from flask.views import MethodView
from kafka import KafkaProducer
from kafka.errors import KafkaTimeoutError

from import_api_app.app import app
from import_api_app.helpers.configuration import ConfigurationFileHandler


class ImportProcessStart(MethodView):
    def __init__(self):
        self.host = app.config["sftp_host"]
        self.port = app.config["sftp_port"]
        self.user = app.config["sftp_user"]
        self.password = app.config["sftp_password"]
        self.base_path = app.config["sftp_base_path"]
        self.topic = app.config["topic-configs"]
        self.kafka_broker_url = app.config["kafka-broker-url"]

        self.mapping_file_handler = ConfigurationFileHandler(
            self.host,
            self.port,
            self.user,
            self.password,
            self.base_path,
            self.kafka_broker_url,
            self.topic,
            app.config["kafka-ssl-ca-file"],
            app.config["kafka-ssl-cert-file"],
            app.config["kafka-ssl-key-file"],
            app.logger,
        )
        self.producer = KafkaProducer(
            bootstrap_servers=app.config["kafka-broker-url"],
            key_serializer=str.encode,
            security_protocol="SSL",
            ssl_cafile=app.config["kafka-ssl-ca-file"],
            ssl_certfile=app.config["kafka-ssl-cert-file"],
            ssl_keyfile=app.config["kafka-ssl-key-file"],
        )

    @swag_from("ImportProcessStart.yml")
    def post(self, institution_id, record_set_id):
        # get parameters of request-body
        request_data = request.json
        job_parameters = request_data["job-parameters"]

        if job_parameters is None:
            return {
                "status": "FATAL",
                "message": "Define all job parameters to start this job!.",
            }, 500
        if "drupalJobUuid" not in job_parameters:
            return {"status": "FATAL", "message": "Need a drupalJobUuid defined."}, 500
        if "drupalJobLogResultUuid" not in job_parameters:
            return {
                "status": "FATAL",
                "message": "Need a drupalJobLogResultUuid defined.",
            }, 500
        if "sessionId" not in job_parameters:
            job_parameters["sessionId"] = uuid.uuid4()
        if "isPublished" not in job_parameters:
            job_parameters["isPublished"] = "false"
        if "xmlRecordTag" not in job_parameters:
            job_parameters["xmlRecordTag"] = "PLACEHOLDER"
        if "xmlIdentifierFieldName" not in job_parameters:
            job_parameters["xmlIdentifierFieldName"] = "PLACEHOLDER"
        if "tableSheetIndex" not in job_parameters:
            job_parameters["tableSheetIndex"] = "1"
        if "tableHeaderCount" not in job_parameters:
            job_parameters["tableHeaderCount"] = "1"
        if "tableHeaderIndex" not in job_parameters:
            job_parameters["tableHeaderIndex"] = "1"
        if "tableIdentifierIndex" not in job_parameters:
            job_parameters["tableIdentifierIndex"] = "1"

        # send mapping files to result:
        mapping_file_result = self.mapping_file_handler.process_mapping_files(
            record_set_id
        )
        if mapping_file_result["status"] == "FATAL":
            return mapping_file_result, 500

        # start text-file-validation
        short_session_id = hashlib.sha1(
            job_parameters["sessionId"].encode("UTF-8")
        ).hexdigest()[:10]
        return self._send_config_to_kafka(key=record_set_id,
                                          topic=app.config["tfv-topic-name"],
                                          content=os.path.join(self.base_path, record_set_id).encode('UTF-8'),
                                          headers=
                                          [
                                              ("recordSetId", record_set_id.encode("UTF-8")),
                                              ("sessionId", job_parameters["sessionId"].encode("UTF-8")),
                                              ("shortSessionId", short_session_id.encode("UTF-8")),
                                              ("institutionId", institution_id.encode("UTF-8")),
                                              ("isPublished", str(job_parameters["isPublished"]).encode("UTF-8")),
                                              ("xmlRecordTag", job_parameters["xmlRecordTag"].encode("UTF-8")),
                                              ("xmlIdentifierFieldName", job_parameters["xmlIdentifierFieldName"].encode("UTF-8")),
                                              ("tableSheetIndex", str(job_parameters["tableSheetIndex"]).encode("UTF-8")),
                                              ("tableHeaderCount", str(job_parameters["tableHeaderCount"]).encode("UTF-8")),
                                              ("tableHeaderIndex", str(job_parameters["tableHeaderIndex"]).encode("UTF-8")),
                                              ("tableIdentifierIndex", str(job_parameters["tableIdentifierIndex"]).encode("UTF-8")),
                                          ],
                                          session_id=job_parameters["sessionId"],
                                          )

    def _send_config_to_kafka(
            self, key: str, topic: str, content: bytes, headers: List[Tuple[str, bytes]], session_id: str
    ) -> Dict[str, Union[str, list]]:
        try:
            # TODO: This could be improved to actually check if the returned future succeeds.
            # However this was never a problem so far.
            self.producer.send(topic, key=key, value=content, headers=headers)
            return {"status": "SUCCESS",
                    "message": f"Started import process for record set {key} with session id {session_id}."}
        except KafkaTimeoutError:
            return {
                "status": "FATAL",
                "type": "KafkaTimeOut",
                "message": f"Kafka Error: {traceback.format_exc()}.",
            }
        except AssertionError:
            return {
                "status": "FATAL",
                "type": "AssertionError",
                "message": f"Kafka Error: {traceback.format_exc()}.",
            }
