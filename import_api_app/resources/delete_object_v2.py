# Import API Service
# Copyright (C) 2020-2021 Memobase Project
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from flask import request
from flask.views import MethodView

from import_api_app.app import app
from import_api_app.helpers.delete_service import DeleteService


class DeleteObjectV2(MethodView):
    def __init__(self):
        self.delete_service = DeleteService(
            app.logger, app.config["NAMESPACE"], app.root_path
        )

    def post(self, session_id, dryrun):
        """
        Tells the deleter service to remove a list of objects
        (either records, recordSets, institutions or a mix)
        ---
        tags:
          - delete service
        parameters:
          - in: path
            name: session_id
            required: true
            description: A session id used to distinguish between different runs.
                         Maximum 50 characters, dash and alphanumeric characters allowed
            example: uuid
            type: string
          - in: path
            name: dryrun
            required: false
            description: True if you want to do a dryrun, otherwise False
            example: True
            type: boolean
            default: False
          - in: body
            name: body
            schema:
              properties:
                ids:
                  type: list
                  example: [sik, aag-001-abcfwef, baz-001]
                  description: The ids of the objects to delete
                recursive_delete:
                  type: boolean
                  required: false
                  description: true if you want to remove the record set object as well,
                               otherwise false
        responses:
          200:
            description: Delete service successfully deployed by Helm on Kubernetes.
                         This however doesn't yet guarantee that the delete workflow
                         as a whole will be successful
            schema:
              properties:
                returncode:
                  type: integer
                  minimum: 0
                  maximum: 0
                  description: Return code given by Helm. Should always be 0.
                  example: 0
                message:
                  type: string
                  description: Information given by Helm on stdout
                  example: "NAME: dd-delete-service-stage-test --
                            LAST DEPLOYED: Mon Jul 18 08:13:28 2022 --
                            NAMESPACE: memobase -- STATUS: deployed --
                            REVISION: 1 -- TEST SUITE: None"
          500:
            description: Starting delete service failed
            schema:
              properties:
                returncode:
                  type: integer
                  minimum: 1
                  description: Return code given by Helm. Should always be greater than 0.
                  example: 2
                message:
                  type: string
                  description: Information given by Helm on stderr
                  example: "Error: cannot re-use a name that is still in use"
        """
        ids = request.json["ids"]
        recursive_delete = request.json["recursive_delete"]
        return self.delete_service.delete_object_v2(
            ids, session_id, dryrun == "true", recursive_delete
        )
