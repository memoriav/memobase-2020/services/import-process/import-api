# Import API Service
# Copyright (C) 2020-2021 Memobase Project
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from flask.views import MethodView
from flask import request

from import_api_app.app import app
from import_api_app.helpers.delete_service import DeleteService


class DeleteRecordset(MethodView):
    def __init__(self):
        self.delete_service = DeleteService(
            app.logger, app.config["NAMESPACE"], app.root_path
        )

    def post(self, session_id, dryrun):
        """
        DEPRECATED. Tells the import process deleter to delete a recordset
        ---
        deprecated: true
        tags:
          - delete service
        parameters:
          - in: path
            name: session_id
            required: true
            description: A session id used to distinguish between different runs.
            example: uuid
            type: string
          - in: path
            name: dryrun
            required: false
            description: True if you want to do a dryrun, otherwise False
            example: True
            type: boolean
            default: False
          - in: body
            name: body
            schema:
              id: recordset_id
              properties:
                recordset_id:
                  type: string
                  example: sik-001
                  description: The id of the recordset to delete
        responses:
          200:
            description: Success, recordset to delete has been found and delete service called
            schema:
              properties:
                status:
                  type: string
                  example: SUCCESS/FAILURE
                  enum: ['SUCCESS', 'FAILURE']

        """
        recordset_id = request.json["recordset_id"]
        return self.delete_service.delete_recordset(
            recordset_id, session_id, dryrun == "true"
        )
