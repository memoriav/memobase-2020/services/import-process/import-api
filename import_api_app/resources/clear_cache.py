# Import API Service
# Copyright (C) 2020-2021 Memobase Project
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import requests
from flask.views import MethodView
from requests.auth import HTTPBasicAuth

from import_api_app.app import app


class ClearCache(MethodView):
    def __init__(self):
        self.logger = app.logger
        self.headers = {"X-API-Key": app.config["drupal-api-key"]}
        user = app.config["drupal-user"]
        password = app.config["drupal-password"]
        self.auth = HTTPBasicAuth(user, password)
        self.url = app.config["drupal-api-url"] + app.config["clear-cache-url"]

    def get(self):
        """
        Tell drupal to clear the import-process-cache
        ---
        tags:
          - clear drupal cache
        responses:
          200:
            description: Success, the call to drupal could be executed
          500:
            description: Failure tha call to drupal could neo be executed
        """

        try:
            response = requests.get(self.url, headers=self.headers, auth=self.auth)
        except Exception as ex:
            msg = "Exception while calling " + self.url + ": " + str(ex)
            self.logger.error(msg)
            return {
                "response": msg,
            }, 500
        if response.ok:
            self.logger.debug("successfully called " + self.url)
            return {
                "content": response.content.decode("utf-8"),
            }, response.status_code
        else:
            self.logger.error("Clearing Cache Failed")
            self.logger.error(
                "statuscode: "
                + str(response.status_code)
                + " content: "
                + response.content.decode("utf-8")
            )
            return {
                "content": response.content.decode("utf-8"),
            }, response.status_code
