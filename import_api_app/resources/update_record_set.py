# Import API Service
# Copyright (C) 2020-2021 Memobase Project
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import json
import os
import uuid

import requests
from flask.views import MethodView
from kafka import KafkaProducer
from kafka.errors import KafkaTimeoutError
from requests.auth import HTTPBasicAuth

from import_api_app.app import app


class UpdateRecordSet(MethodView):
    def __init__(self):
        self.producer = KafkaProducer(
            bootstrap_servers=app.config["kafka-broker-url"],
            value_serializer=lambda m: json.dumps(m, ensure_ascii=False).encode(
                "utf-8"
            ),
            security_protocol=app.config["kafka-security-protocol"],
            ssl_check_hostname=True,
            ssl_cafile=app.config["kafka-ssl-ca-location"],
            ssl_certfile=app.config["kafka-ssl-certificate-location"],
            ssl_keyfile=app.config["kafka-ssl-key-location"],
        )
        self.base_url = app.config["drupal-api-url"]
        self.json_api_path = "/jsonapi/node/record_set/"
        self.institution_path = "/jsonapi/node/institution/"
        self.metadata_language_path = "/jsonapi/taxonomy_term/language_of_metadata/"
        self.headers = {"X-API-Key": app.config["drupal-api-key"]}
        user = app.config["drupal-user"]
        password = app.config["drupal-password"]
        self.auth = HTTPBasicAuth(user, password)

    def get(self, record_set_drupal_uuid):
        """
        Update the record set with the given drupal UUID in the backend.
        ---
        tags:
          - Import Record Set
        parameters:
          - in: path
            name: record_set_drupal_uuid
            required: true
            description: The UUID of the updated recordset
            example: 0c4c777c-94f8-45ba-945a-bfe6967d40da
            type: string
        responses:
          200:
            description: Success, the information has been written into
                the kafka topic
            schema:
              properties:
                status:
                  type: string
                  example: SUCCESS/FAILURE
                  enum: ['SUCCESS', 'FAILURE']
                result_topic_value:
                  type: string/json
                  example: the value written into the topic

        """
        result_topic_value = ""
        try:
            url = (
                os.environ["INGEST_API_URL"]
                + f"/api/v1/recordset/{record_set_drupal_uuid}"
            )
            app.logger.info(f"Sending request to new ingest service: {url}")
            response = requests.put(url)
            app.logger.info(f"Response: {response.text}")
        except Exception as ex:
            app.logger.error(f"Exception with ingest service: {ex}")

        # Retrieve Drupal Entities for each language.
        # Returns default entity if there is a language missing.
        de_drupal_url = (
            f"{self.base_url}/de{self.json_api_path}{record_set_drupal_uuid}"
        )
        fr_drupal_url = (
            f"{self.base_url}/fr{self.json_api_path}{record_set_drupal_uuid}"
        )
        it_drupal_url = (
            f"{self.base_url}/it{self.json_api_path}{record_set_drupal_uuid}"
        )

        try:
            app.logger.info(f"Fetching data from {de_drupal_url}")
            drupal_record_set_de_json = requests.get(
                de_drupal_url, headers=self.headers, auth=self.auth
            ).json()
            app.logger.info(drupal_record_set_de_json)
            drupal_record_set_de = drupal_record_set_de_json["data"]
            app.logger.info(f"Fetching data from {fr_drupal_url}")
            drupal_record_set_fr_json = requests.get(
                fr_drupal_url, headers=self.headers, auth=self.auth
            ).json()
            app.logger.info(drupal_record_set_fr_json)
            drupal_record_set_fr = drupal_record_set_fr_json["data"]
            app.logger.info(f"Fetching data from {it_drupal_url}")
            drupal_record_set_it_json = requests.get(
                it_drupal_url, headers=self.headers, auth=self.auth
            ).json()
            app.logger.info(drupal_record_set_it_json)
            drupal_record_set_it = drupal_record_set_it_json["data"]

            institutions = drupal_record_set_de["relationships"]["field_institution"][
                "data"
            ]
            institution_ids = self.get_institution_id_list(institutions)

            institutions = drupal_record_set_de["relationships"][
                "field_resp_institution_access"
            ]["data"]
            access_institution_ids = self.get_institution_id_list(institutions)

            institutions = drupal_record_set_de["relationships"][
                "field_resp_institution_master"
            ]["data"]
            master_institutions_ids = self.get_institution_id_list(institutions)

            institutions = drupal_record_set_de["relationships"][
                "field_resp_institution_original"
            ]["data"]
            original_institution_ids = self.get_institution_id_list(institutions)

            metadata_language_codes = []
            metadata_languages = drupal_record_set_de["relationships"][
                "field_metadata_languages"
            ]["data"]
            for metadata_language in metadata_languages:
                drupal_record_set_language_code = requests.get(
                    f'{self.base_url}{self.metadata_language_path}{metadata_language["id"]}',
                    headers=self.headers,
                    auth=self.auth,
                )
                metadata_language_codes.append(
                    drupal_record_set_language_code.json()["data"]["attributes"]["name"]
                )

            related_record_sets_de = UpdateRecordSet.get_related_record_sets(
                drupal_record_set_de
            )
            related_record_sets_fr = UpdateRecordSet.get_related_record_sets(
                drupal_record_set_fr
            )
            related_record_sets_it = UpdateRecordSet.get_related_record_sets(
                drupal_record_set_it
            )

            result_topic_value = {
                "type": drupal_record_set_de["type"],
                "status": drupal_record_set_de["attributes"]["status"],
                "title_de": drupal_record_set_de["attributes"]["title"],
                "title_fr": drupal_record_set_fr["attributes"]["title"],
                "title_it": drupal_record_set_it["attributes"]["title"],
                "field_institution": institution_ids,
                "field_metadata_language_codes": metadata_language_codes,
                "computed_teaser_image_url": drupal_record_set_de["attributes"][
                    "computed_teaser_image_url"
                ],
                "field_processed_teaser_text_de": drupal_record_set_de["attributes"][
                    "field_processed_teaser_text"
                ],
                "field_processed_teaser_text_fr": drupal_record_set_fr["attributes"][
                    "field_processed_teaser_text"
                ],
                "field_processed_teaser_text_it": drupal_record_set_it["attributes"][
                    "field_processed_teaser_text"
                ],
                "field_old_memobase_id": drupal_record_set_de["attributes"][
                    "field_old_memobase_id"
                ],
                "field_access_de": drupal_record_set_de["attributes"]["field_access"],
                "field_access_fr": drupal_record_set_fr["attributes"]["field_access"],
                "field_access_it": drupal_record_set_it["attributes"]["field_access"],
                "field_access_memobase_de": drupal_record_set_de["attributes"][
                    "field_access_memobase"
                ],
                "field_access_memobase_fr": drupal_record_set_fr["attributes"][
                    "field_access_memobase"
                ],
                "field_access_memobase_it": drupal_record_set_it["attributes"][
                    "field_access_memobase"
                ],
                "field_content_de": drupal_record_set_de["attributes"]["field_content"],
                "field_content_fr": drupal_record_set_fr["attributes"]["field_content"],
                "field_content_it": drupal_record_set_it["attributes"]["field_content"],
                "field_context_de": drupal_record_set_de["attributes"]["field_context"],
                "field_context_fr": drupal_record_set_fr["attributes"]["field_context"],
                "field_context_it": drupal_record_set_it["attributes"]["field_context"],
                "field_data_transfer_de": drupal_record_set_de["attributes"][
                    "field_data_transfer"
                ],
                "field_data_transfer_fr": drupal_record_set_fr["attributes"][
                    "field_data_transfer"
                ],
                "field_data_transfer_it": drupal_record_set_it["attributes"][
                    "field_data_transfer"
                ],
                "field_documents_de": drupal_record_set_de["attributes"][
                    "field_documents"
                ],
                "field_documents_fr": drupal_record_set_fr["attributes"][
                    "field_documents"
                ],
                "field_documents_it": drupal_record_set_it["attributes"][
                    "field_documents"
                ],
                "field_info_on_development_de": drupal_record_set_de["attributes"][
                    "field_info_on_development"
                ],
                "field_info_on_development_fr": drupal_record_set_fr["attributes"][
                    "field_info_on_development"
                ],
                "field_info_on_development_it": drupal_record_set_it["attributes"][
                    "field_info_on_development"
                ],
                "field_language_de": drupal_record_set_de["attributes"][
                    "field_language"
                ],
                "field_language_fr": drupal_record_set_fr["attributes"][
                    "field_language"
                ],
                "field_language_it": drupal_record_set_it["attributes"][
                    "field_language"
                ],
                "field_memobase_id": drupal_record_set_de["attributes"][
                    "field_memobase_id"
                ],
                "field_notes": drupal_record_set_de["attributes"]["field_notes"],
                "field_original_description_de": drupal_record_set_de["attributes"][
                    "field_original_description"
                ],
                "field_original_description_fr": drupal_record_set_fr["attributes"][
                    "field_original_description"
                ],
                "field_original_description_it": drupal_record_set_it["attributes"][
                    "field_original_description"
                ],
                "field_original_id": drupal_record_set_de["attributes"][
                    "field_original_id"
                ],
                "field_original_shelf_mark": drupal_record_set_de["attributes"][
                    "field_original_shelf_mark"
                ],
                "field_original_title_de": drupal_record_set_de["attributes"][
                    "field_original_title"
                ],
                "field_original_title_fr": drupal_record_set_fr["attributes"][
                    "field_original_title"
                ],
                "field_original_title_it": drupal_record_set_it["attributes"][
                    "field_original_title"
                ],
                "field_project_de": drupal_record_set_de["attributes"]["field_project"],
                "field_project_fr": drupal_record_set_fr["attributes"]["field_project"],
                "field_project_it": drupal_record_set_it["attributes"]["field_project"],
                "field_publications_de": drupal_record_set_de["attributes"][
                    "field_publications"
                ],
                "field_publications_fr": drupal_record_set_fr["attributes"][
                    "field_publications"
                ],
                "field_publications_it": drupal_record_set_it["attributes"][
                    "field_publications"
                ],
                "field_related_record_sets_de": related_record_sets_de,
                "field_related_record_sets_fr": related_record_sets_fr,
                "field_related_record_sets_it": related_record_sets_it,
                "field_rights_de": drupal_record_set_de["attributes"]["field_rights"],
                "field_rights_fr": drupal_record_set_fr["attributes"]["field_rights"],
                "field_rights_it": drupal_record_set_it["attributes"]["field_rights"],
                "field_scope_de": drupal_record_set_de["attributes"]["field_scope"],
                "field_scope_fr": drupal_record_set_fr["attributes"]["field_scope"],
                "field_scope_it": drupal_record_set_it["attributes"]["field_scope"],
                "field_selection_de": drupal_record_set_de["attributes"][
                    "field_selection"
                ],
                "field_selection_fr": drupal_record_set_fr["attributes"][
                    "field_selection"
                ],
                "field_selection_it": drupal_record_set_it["attributes"][
                    "field_selection"
                ],
                "field_supported_by_memoriav": drupal_record_set_de["attributes"][
                    "field_supported_by_memoriav"
                ],
                "field_time_period": drupal_record_set_de["attributes"][
                    "field_time_period"
                ],
                "field_transfer_date": drupal_record_set_de["attributes"][
                    "field_transfer_date"
                ],
                "field_image_gallery": drupal_record_set_de["relationships"][
                    "field_image_gallery"
                ],
                "field_resp_institution_access": access_institution_ids,
                "field_resp_institution_master": master_institutions_ids,
                "field_resp_institution_original": original_institution_ids,
                "field_teaser_image": drupal_record_set_de["relationships"][
                    "field_teaser_image"
                ],
            }
        except LookupError as ex:
            msg = "LookupError for " + record_set_drupal_uuid + ": " + str(ex)
            app.logger.error(msg)
            return {
                "status": "FAILURE",
                "topic_key": result_topic_value.get("field_memobase_id"),
                "result_topic_value": result_topic_value,
            }, 500
        except Exception as ex:
            msg = "Exception for " + record_set_drupal_uuid + ": " + str(ex)
            app.logger.error(msg)
            return {
                "status": "FAILURE",
                "topic_key": result_topic_value.get("field_memobase_id"),
                "result_topic_value": result_topic_value,
            }, 500

        return self.send_message(result_topic_value, record_set_drupal_uuid)

    def send_message(self, result_topic_value, record_set_drupal_id):
        headers = [
            (
                "recordSetId",
                bytes(
                    result_topic_value.get("field_memobase_id"),
                    encoding="utf-8",
                ),
            ),
            ("sessionId", bytes(str(uuid.uuid4()), encoding="utf-8")),
            ("institutionId", bytes("none", encoding="utf-8")),
            (
                "isPublished",
                bytes(str(result_topic_value["status"]), encoding="utf-8"),
            ),
        ]
        try:
            key = bytes(result_topic_value.get("field_memobase_id"), encoding="utf-8")
            app.logger.debug(
                f"Send message: key={key}, headers={headers}, "
                f"message: {json.dumps(result_topic_value, ensure_ascii=False)}"
            )
            self.producer.send(
                app.config["topic-drupal-export-record-set"],
                result_topic_value,
                key=key,
                headers=headers,
            )
        except KafkaTimeoutError as ex:
            msg = f"KafkaTimeoutError ({record_set_drupal_id}): {ex}."
            app.logger.error(msg)
            return {
                "status": "FAILURE",
                "topic_key": result_topic_value.get("field_memobase_id"),
                "result_topic_value": result_topic_value,
                "exception": msg,
            }, 503
        except Exception as ex:
            msg = (
                "Could not import"
                f" {result_topic_value.get('field_memobase_id')} (Drupal UUID:"
                f" {record_set_drupal_id}) (Unknown Exception): " + str(ex)
            )
            app.logger.error(msg)
            return {
                "status": "FAILURE",
                "topic_key": result_topic_value.get("field_memobase_id"),
                "result_topic_value": result_topic_value,
                "exception": msg,
            }, 503

        app.logger.debug("success for " + record_set_drupal_id)
        return {
            "status": "SUCCESS",
            "topic_key": result_topic_value.get("field_memobase_id"),
            "result_topic_value": result_topic_value,
        }, 200

    def get_institution_id_list(self, institution_data):
        institution_ids = []
        for institution in institution_data:
            url = f"{self.base_url}{self.institution_path}{institution['id']}"
            drupal_institution = requests.get(url, headers=self.headers, auth=self.auth)
            institution_ids.append(
                drupal_institution.json()["data"]["attributes"]["field_memobase_id"]
            )
        return institution_ids

    @staticmethod
    def get_related_record_sets(fields):
        related_record_sets = fields["attributes"]["field_related_record_sets"]
        revised_related_record_sets = []
        for related_record_set in related_record_sets:
            if "entity:node" in related_record_set["uri"]:
                related_record_set["uri"] = fields["attributes"]["field_memobase_id"]
                # This is a hard coded value. If you change this it needs to be changed
                # in the drupal sync service as well.
                related_record_set["title"] = "internal"
            revised_related_record_sets.append(related_record_set)
        return revised_related_record_sets
