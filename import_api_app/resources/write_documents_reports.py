# Import API Service
# Copyright (C) 2020-2021 Memobase Project
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import json
from datetime import datetime

import requests
from flask.views import MethodView
from flask import request
from import_api_app.app import app
from import_api_app.helpers.error import ImportApiError
from requests.auth import HTTPBasicAuth


class WriteJobResultToDrupal(MethodView):
    def __init__(self):
        self.logger = None
        self.headers = {
            "Content-Type": "application/vnd.api+json",
            "Accept": "application/vnd.api+json",
            "X-API-Key": app.config["drupal-api-key"],
        }
        user = app.config["drupal-user"]
        password = app.config["drupal-password"]
        self.auth = HTTPBasicAuth(user, password)
        self.job_url = f'{app.config["drupal-api-url"]}/jsonapi/node/log_result/'
        self.import_process_endpoint = (
            f'{app.config["drupal-api-url"]}' f"/jsonapi/node/import_process/"
        )

    def post(self, job_drupal_uuid, job_log_drupal_uuid):
        """
        Write the job summary to Drupal job log (in field_summary)
        ---
        tags:
          - reporting
        parameters:
        - in: path
          name: job_drupal_uuid
          required: true
          description: job log uuid
          type: string
        - in: path
          name: job_log_drupal_uuid
          required: true
          description: job report log uuid
          type: string
        - in: body
          name: body
          required: true
          schema:
              properties:
                sessionId:
                    type: string
                    example: "550e8400-e29b-11d4-a716-446655440000"
                step:
                    type: string
                    example: "media-metadata-extractor"
                total:
                    type: integer
                    example: 468
                success:
                    type: integer
                    example: 413
                ignore:
                    type: integer
                    example: 0
                warning:
                    type: integer
                    example: 455
                fatal:
                    type: integer
                    example: 0
                earliest:
                    type: string
                    example: "2021-03-08T14:02:23.232"
                latest:
                    type: string
                    example: "latest"
                elapsedTime:
                    type: string
                    example: "00:03:11.219"
                recordSetId:
                    type: string
                    example: "fss-001"
                institutionId:
                    type: string
                    example: "fss"
                messageId:
                    type: string
                    example: "1b7d8224-451a-4afb-b5d2-6537acb74051"
                previousMessageId:
                    type: string
                    example: "04543477-3bb5-4d01-a318-52ded1ce5e1c"
                version:
                    type: string
                    example: "3"
        responses:
          200:
            description: It was successful
          404:
            description: No log result with such a uuid
          500:
            description: There was a problem
        """
        self.logger = app.logger
        body = request.json
        app.logger.debug("Report Input Data: \n" + json.dumps(body, indent=2))
        try:
            fatal = body["fatal"]
            if fatal == 0:
                status = "SUCCESS"
            else:
                status = "FAILED"
            return self.write_results(
                job_drupal_uuid, job_log_drupal_uuid, status, body
            )
        except ImportApiError as e:
            self.logger.error(e)
            return {"error": e.message}, 500

    def write_results(self, job_drupal_uuid, job_log_drupal_uuid, status, report):
        self.logger.debug("Job UUID: " + job_drupal_uuid)
        self.logger.debug("Job Log Report UUID: " + job_log_drupal_uuid)
        get_url = self.job_url + job_log_drupal_uuid
        try:
            response = requests.get(
                get_url, headers=self.headers, auth=self.auth, timeout=20
            )
        except requests.exceptions.Timeout:
            self.logger.error(f"Timeout (>20s) for GET request on {get_url}")
            return
        if response.ok:
            try:
                job_log = response.json()
            except json.decoder.JSONDecodeError as ex:
                self.logger.error(f"Could not parse response as JSON: {response.text}.")
                raise ImportApiError(ex)
            log_data = job_log["data"]
            previous_status = log_data["attributes"]["field_status"]
            previous_report = log_data["attributes"]["field_message"]
        else:
            raise ImportApiError(response.text)

        if previous_status == "FAILED":
            status = previous_status
        if previous_report is None or previous_report == "null":
            previous_report_json = {}
        else:
            previous_report_json = json.loads(previous_report)
        step = report["step"]
        previous_report_json[step] = report
        report_as_string = json.dumps(previous_report_json)

        # updating the status of the import process.
        patch_calls = {}
        patch_url = self.import_process_endpoint + job_drupal_uuid
        patch_data = {
            "data": {
                "id": job_drupal_uuid,
                "type": "node--import_process",
                "attributes": {"field_state": 0},
            }
        }
        # updating the content of the job log entity
        patch_calls[patch_url] = patch_data
        patch_url = self.job_url + job_log_drupal_uuid
        patch_data = {
            "data": {
                "id": job_log_drupal_uuid,
                "type": "node--job_result",
                "attributes": {
                    "field_end_date": datetime.utcnow().strftime(
                        "%Y-%m-%dT%H:%M:%S+00:00"
                    ),
                    "field_status": status,
                    "field_message": report_as_string,
                },
            }
        }
        patch_calls[patch_url] = patch_data
        result = {"message": ""}

        for patchCall in patch_calls:
            url = patchCall
            data = patch_calls[patchCall]
            try:
                response = requests.patch(
                    url,
                    headers=self.headers,
                    data=json.dumps(data),
                    auth=self.auth,
                    timeout=20,
                )
            except requests.exceptions.RequestException:
                message = (
                    "It was not possible to write to Drupal API \
                          via the following url "
                    + url
                )
                app.logger.error(message)
                raise ImportApiError(message)
            if response.status_code == 200:
                app.logger.debug("Updated: " + url)
                result["message"] += "Updated: " + url + "\n"
            elif response.status_code == 401:
                message = f"Unauthorized access on {url}"
                app.logger.error(message)
                raise ImportApiError(message)
            elif response.status_code == 403:
                message = "Not authorized to write to: " + url
                app.logger.error(message)
                raise ImportApiError(message)
            elif response.status_code == 404:
                message = "Not Found: " + url
                app.logger.error(message)
                raise ImportApiError(message)
            elif response.status_code == 500:
                message = (
                    "There was an internal server error for "
                    + url
                    + ": "
                    + str(response.text)
                    + ". Check the logs for details."
                )
                app.logger.error(message)
                raise ImportApiError(message)
            else:
                message = (
                    f"Unknown response status code {response.status_code} "
                    f"({response.reason}) for drupal api for url " + url
                )
                app.logger.error(message)
                raise ImportApiError(message)
        return result
