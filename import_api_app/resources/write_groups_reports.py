# Import API Service
# Copyright (C) 2020-2021 Memobase Project
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import json

import requests
from flask.views import MethodView
from flask import request
from requests.auth import HTTPBasicAuth

from import_api_app.app import app
from import_api_app.helpers.error import ImportApiError


class WriteTypeReportToDrupal(MethodView):
    def __init__(self):
        self.base_url = app.config["drupal-api-url"]
        self.headers = {
            "Content-Type": "application/vnd.api+json",
            "Accept": "application/vnd.api+json",
            "X-API-Key": app.config["drupal-api-key"],
        }
        user = app.config["drupal-user"]
        password = app.config["drupal-password"]
        self.auth = HTTPBasicAuth(user, password)

    def post(self):
        """
        Write a report for an recordset or institution Drupal
        ---
        tags:
          - reporting
        parameters:
            - in: body
              name: body
              required: true
              schema:
                  item:
                      type: object
                      properties:
                          node_type:
                              type: string
                              example: 'record_set'
                              description: Either 'record_set' or 'institution'.
                          id:
                              type: string
                              example: 'sag'
                              description: The memobase id of the record set or institution.
                          status:
                              type: boolean
                              example: true
                              description: The status
                          report:
                              type: string
                              example: "Failed to process record set."
                              description: Text message of the report.
        responses:
            200:
                description: It was successful
            404:
                description: No content with such a id found
            500:
                description: There was a problem
        """
        try:
            node_type = request.json["node_type"]
            identifier = request.json["id"]
            status = request.json["status"]
            report = request.json["report"]
        except KeyError as er:
            return {"message": f"Missing a element in input body: {er}"}, 400
        try:
            node_id, lang_code = self.get_drupal_uuid(identifier, node_type)
            return self.write_results(node_type, node_id, lang_code, status, report)
        except ImportApiError as e:
            app.logger.error(
                f"Could not write report to {node_type}/{identifier}: {e.message}"
            )
            return {
                "error": f"Could not write report to {node_type}/{identifier}: "
                f"{e.message}"
            }, 500

    def get_drupal_uuid(self, memobase_id: str, node_type: str):
        url = f"{self.base_url}/jsonapi/node/{node_type}?filter[field_memobase_id]={memobase_id}"
        response = requests.get(url, headers=self.headers, auth=self.auth)
        if response.ok:
            try:
                result = response.json()["data"][0]
                return result["id"], result["attributes"]["langcode"]
            except KeyError as er:
                app.logger.error(
                    f"Could not find the key {er} in data: {response.text}."
                )
                raise ImportApiError(
                    f"Could not find the key {er} in data (url={url}): "
                    f"{response.text}."
                )
            except IndexError:
                app.logger.error(
                    f"Data field does not contain a list (url={url}): "
                    f"{response.text}."
                )
                raise ImportApiError(
                    f"Data field does not contain a list (url={url}): "
                    f"{response.text}."
                )
        else:
            app.logger.error("Could not find element: " + url)
            raise ImportApiError(f"Could not find element for report: {response.text}.")

    def write_results(self, node_type, identifier, lang_code, status, report):
        app.logger.debug(
            f"writing: {lang_code}/{node_type}/{str(identifier)}"
            f"/{str(status)}/{report}"
        )

        drupal_type = ""
        url = f"{self.base_url}/{lang_code}/jsonapi/node/"
        if node_type == "institution":
            drupal_type = "node--institution"
            url += "institution/"
            url = f"{url}{identifier}"
        elif node_type == "record_set":
            drupal_type = "node--record_set"
            url += "record_set/"
            url = f"{url}{identifier}"

        data = {
            "data": {
                "id": identifier,
                "type": drupal_type,
                "attributes": {
                    "field_migrated": (
                        1
                        if status == "true" or status is True or status == "True"
                        else 0
                    ),
                    "field_error": report,
                },
            }
        }

        result = {"message": ""}

        try:
            response = requests.patch(
                url, headers=self.headers, auth=self.auth, data=json.dumps(data)
            )
        except requests.exceptions.RequestException:
            message = f"Request Error: {url}"
            app.logger.error(message)
            raise ImportApiError(message)
        if response.ok:
            app.logger.debug("Updated: " + url)
            result["message"] += "Updated: " + url + "\n"
        elif response.status_code == 401:
            message = f"Unauthorized access on {url}"
            app.logger.error(message)
            raise ImportApiError(message)
        elif response.status_code == 403:
            message = f"Authorization Failed: {url}"
            app.logger.error(message)
            raise ImportApiError(message)
        elif response.status_code == 404:
            message = "Not Found: " + url
            app.logger.error(message)
            raise ImportApiError(message)
        else:
            message = (
                f"Unknown response status code {response.status_code} "
                f"({response.reason}) for drupal api for url " + url
            )
            app.logger.error(message)
            raise ImportApiError(message)
        return result
