# Import API Service
# Copyright (C) 2020-2021 Memobase Project
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from flask.views import MethodView

from import_api_app.app import app
from import_api_app.helpers.configuration import ConfigurationFileHandler


class FetchMappingFile(MethodView):
    def __init__(self):
        self.host = app.config["sftp_host"]
        self.port = app.config["sftp_port"]
        self.user = app.config["sftp_user"]
        self.password = app.config["sftp_password"]
        self.base_path = app.config["sftp_base_path"]
        self.topic = app.config["topic-configs"]
        self.kafka_broker_url = app.config["kafka-broker-url"]

        self.mapping_file_handler = ConfigurationFileHandler(
            self.host,
            self.port,
            self.user,
            self.password,
            self.base_path,
            self.kafka_broker_url,
            self.topic,
            app.config["kafka-ssl-ca-file"],
            app.config["kafka-ssl-cert-file"],
            app.config["kafka-ssl-key-file"],
            app.logger,
        )

    def get(self, recordset_id, session_id):
        """
        Fetches the mapping file from the sFTP server
        ---
        tags:
          - fetch mapping file
        parameters:
          - in: path
            name: recordset_id
            required: true
            description: The name of the record set (matches folder name on sftp)
            example: AfZ-Becker-Audiovisuals
            type: string
          - in: path
            name: session_id
            required: true
            description: A session id used to distinguish between different runs.
            example: uuid
            type: string
        responses:
          200:
            description: Success, the mapping file has been retrieved
            schema:
              properties:
                status:
                  type: string
                  example: SUCCESS/FAILURE
                  enum: ['SUCCESS', 'FAILURE']
                contents:
                  type: string/yml
                  example: the contents of the mapping file...

        """
        result = self.mapping_file_handler.process_mapping_files(recordset_id)
        if result["status"] == "SUCCESS":
            return result, 200
        else:
            return result, 500
