# Import API Service
# Copyright (C) 2020-2021 Memobase Project
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from flask.views import MethodView
from flask import request

from import_api_app.app import app
from import_api_app.helpers.delete_service import DeleteService


class DeleteRecord(MethodView):
    def __init__(self):
        self.delete_service = DeleteService(
            app.logger, app.config["NAMESPACE"], app.root_path
        )

    def post(self, session_id, dryrun):
        """
        DEPRECATED: Tells the import process deleter to delete a record
        ---
        deprecated: true
        tags:
          - delete service
        parameters:
          - in: path
            name: session_id
            required: true
            description: A session id used to distinguish between different runs.
            example: uuid
            type: string
          - in: path
            name: dryrun
            required: false
            description: True if you want to do a dryrun, otherwise False
            example: True
            type: boolean
            default: False
          - in: body
            name: body
            schema:
              id: record_id
              properties:
                record_id:
                  type: string
                  example: https://memobase.ch/record/sik-001-12
                  description: The id of the record to delete
        responses:
          200:
            description: Success, record to delete has been found and delete service called
            schema:
              properties:
                status:
                  type: string
                  example: SUCCESS/FAILURE
                  enum: ['SUCCESS', 'FAILURE']

        """
        record_id = request.json["record_id"]
        return self.delete_service.delete_record(
            record_id, session_id, dryrun == "true"
        )
