import subprocess


def install(
    chart_uri,
    name,
    version,
    set_values=None,
    namespace=None,
    fail_on_err=True,
    wait=True,
):
    cmd = ["helm", "install", name, chart_uri, "--version", version]
    if set_values:
        opt_list = ["%s=%s" % (k, v) for k, v in set_values.items()]
        cmd.extend(("--set", ",".join(opt_list)))
    if namespace:
        cmd.extend(("--namespace", namespace))
    if wait:
        cmd.append("--wait")
    return subprocess.run(
        cmd,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        check=fail_on_err,
        text=True,
    )
