# Import API Service
# Copyright (C) 2020-2021 Memobase Project
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from datetime import timezone


def generate_helm_name(process_id, record_set_id, job_name):
    return record_set_id + "-" + process_id + "-" + job_name


def get_job_info(job):
    """ "
    job is a Kubernetes V1Job
    """
    utc_start_time = job.status.start_time
    local_start_time = utc_start_time.replace(tzinfo=timezone.utc).astimezone(tz=None)
    status = "Unknown"
    if job.status.failed:
        status = "Failed"
    elif job.status.active:
        status = "Running"
    return {
        "status": status,
        "started": local_start_time.strftime("%m/%d/%Y, %H:%M:%S"),
    }
