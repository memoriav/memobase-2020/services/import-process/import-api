from import_api_app.app import app
import json
import requests
from requests.auth import HTTPBasicAuth


class DrupalException(Exception):
    def __init__(self, status_code, message):
        self.status_code = status_code
        self.message = message
        super().__init__(message)

    def __str__(self):
        return f"{self.message} (code {self.status_code})"


class ResultException(Exception):
    def __init__(self, message):
        self.message = message
        super().__init__(message)


class InstitutionObjectBuilder:
    """Contains helper functions to fetch data from the Drupal API to build the
    institution object. There are two cases covered:

    1) The main information about an institution are changed

    2) A "pseudo collection" of an institution is changed. Pseudo collections
    are aggregations over a specific media type of which the institution has
    objects
    """

    def __init__(self, config):
        """Initialises an instance of the class

        Args:
            config (dict): Main configuration object of the app
        """

        self.auth = HTTPBasicAuth(config["drupal-user"], config["drupal-password"])
        self.headers = {"X-API-Key": config["drupal-api-key"]}
        self.base_url = config["drupal-api-url"]
        pass

    def get_by_institution_uuid(self, drupal_uuid, get_collections=True) -> dict:
        """
        Should be called when the main information of an institution is updated.

        Args:
            drupal_uuid (str): UUID of the change, issued by Drupal
            get_collections (bool): True if the respective "pseudo collections"
                                    should be fetched as well. Default is True.

        Returns:
            Institution object
        """
        institution_path = f"jsonapi/node/institution/{drupal_uuid}"
        response = {"de": {}, "fr": {}, "it": {}}
        for lang in response.keys():
            if not response[lang]:
                url = f"{self.base_url}/{lang}/{institution_path}"
                response[lang] = self._request_only_data(url)
        addresses = self._get_institution_addresses(response["de"])
        master_institution_types = self._get_master_institution_types(response["de"])
        other_institution_types = self._get_other_institution_types(response["de"])
        record_set_ids = self._get_record_set_ids(drupal_uuid)
        result = self._build_institution_object(
            response,
            addresses,
            master_institution_types,
            other_institution_types,
            record_set_ids,
        )
        if get_collections:
            result["collections"] = self._get_collections_data_by_institution_id(
                response["de"]["attributes"]["field_memobase_id"]
            )
        app.logger.debug("Result object built:\n" + json.dumps(result))
        return result

    def get_by_collections_uuid(self, drupal_uuid):
        """
        Should be called when the data of a "pseudo collection" is changed.

        Args:
            drupal_uuid (str): UUID of the change, issued by Drupal

        Returns:
            Institution object
        """
        url = (
            f"{self.base_url}/de/jsonapi/node/institution_inventar_data/"
            + f"{drupal_uuid}?fields[taxonomy_term--document_type]=name&"
            + "fields[taxonomy_term--time]=name&"
            + "fields[taxonomy_term--subject]=name&fields[taxonomy_term--geo]=name&"
            + "include=field_document_type,field_time,field_geo,field_subjects"
        )
        response = self._request_full_data(url)
        try:
            institution_uuid = response["data"]["relationships"]["field_institution"][
                "data"
            ][0]["id"]
        except KeyError:
            raise ResultException(
                "Object does not contain field 'data.relationships.field_institution.data.0.id'"
            )
        result = self.get_by_institution_uuid(institution_uuid, False)
        institution_id = result["field_memobase_id"]
        result["collections"] = self._get_collections_data_by_institution_id(
            institution_id
        )
        app.logger.debug("Result object built:\n" + json.dumps(result))
        return result

    def _get_collections_data_by_institution_id(self, institution_id):
        url = (
            f"{self.base_url}/de/jsonapi/node/institution_inventar_data?"
            + "filter[field_institution.field_memobase_id]={institution_id}&"
            + "fields[taxonomy_term--document_type]=name&fields[taxonomy_term--time]=name&"
            + "fields[taxonomy_term--subject]=name&fields[taxonomy_term--geo]=name&"
            + "include=field_document_type,field_time,field_geo,field_subjects"
        )
        response = self._request_full_data(url)
        if isinstance(response) != dict:
            raise ResultException(
                f"Response should be of type 'dict'. Is type '{type(response)}' instead"
            )
        return self._build_collections_object(response)

    def _get_institution_addresses(self, result_set_de):
        url = f"{self.base_url}/jsonapi/paragraph/extended_address"
        address_paragraphs = result_set_de["relationships"]["field_extended_address"]
        return self._build_institution_addresses_object(
            [
                self._request_only_data(f"{url}/{element['id']}")
                for element in address_paragraphs["data"]
            ]
        )

    def _get_master_institution_types(self, result_set_de):
        return self._get_institution_types(result_set_de, "field_institution_types")

    def _get_other_institution_types(self, result_set_de):
        return self._get_institution_types(
            result_set_de, "field_other_institution_types"
        )

    def _get_institution_types(self, result_set_de, field_name):
        url = result_set_de["relationships"][field_name]["links"]["related"]["href"]
        response = self._request_only_data(url)
        return self._build_institution_types_object(response)

    def _get_record_set_ids(self, drupal_uuid):
        url = (
            f"{self.base_url}/de/jsonapi/node/record_set?"
            + "filter[field_institution.id][value]={drupal_uuid}"
        )
        result = self._request_only_data(url)
        return self._build_record_set_ids_object(result)

    def _request_full_data(self, url) -> dict:
        app.logger.debug(f"Requesting data from {url}")
        response = requests.get(url, headers=self.headers, auth=self.auth)
        if response.ok:
            json_response = response.json()
            if "errors" in json_response:
                errors = json_response["errors"]
                if len(errors) == 0:
                    status = "<no status code provided>"
                    details = "<no details provided>"
                else:
                    status = (
                        errors[0]["status"]
                        if "status" in errors[0]
                        else "<no status code provided>"
                    )
                    details = (
                        errors[0]["detail"]
                        if "detail" in errors[0]
                        else "<no details provided>"
                    )
                raise DrupalException(status, details)
            return json_response
        else:
            raise DrupalException(response.status_code, response.text)

    def _request_only_data(self, url) -> dict:
        response = self._request_full_data(url)
        if "data" in response:
            return response["data"]
        else:
            raise ResultException("No key 'data' found in json response")

    @staticmethod
    def _build_institution_object(
        response,
        addresses,
        master_institution_types,
        other_institution_types,
        record_set_ids,
    ):
        if (
            "field_wikidata_id" in response["de"]["attributes"]
            and response["de"]["attributes"]["field_wikidata_id"]
        ):
            field_wikidata_id = response["de"]["attributes"]["field_wikidata_id"]["uri"]
        else:
            field_wikidata_id = None
        return {
            "type": response["de"]["type"],
            "status": response["de"]["attributes"]["status"],
            "title_de": response["de"]["attributes"]["title"],
            "title_fr": response["fr"]["attributes"]["title"],
            "title_it": response["it"]["attributes"]["title"],
            "field_address": addresses,
            "field_isil": response["de"]["attributes"]["field_isil"],
            "field_memobase_id": response["de"]["attributes"]["field_memobase_id"],
            "field_old_memobase_id": response["de"]["attributes"][
                "field_old_memobase_id"
            ],
            "field_email": response["de"]["attributes"]["field_email"],
            "field_website": response["de"]["attributes"]["field_website"],
            "field_wikidata_id": field_wikidata_id,
            "field_link_archive_catalog": response["de"]["attributes"][
                "field_link_archive_catalog"
            ],
            "field_text_de": response["de"]["attributes"]["field_text"],
            "field_text_fr": response["fr"]["attributes"]["field_text"],
            "field_text_it": response["it"]["attributes"]["field_text"],
            "field_master_institution_types": master_institution_types,
            "field_other_institution_types": other_institution_types,
            "field_private": response["de"]["attributes"]["field_private"],
            "field_memobase_institution": response["de"]["attributes"][
                "field_memobase_institution"
            ],
            "field_survey_date": response["de"]["attributes"]["field_survey_date"],
            "field_survey_institution": response["de"]["attributes"][
                "field_survey_institution"
            ],
            "field_accessibility_de": response["de"]["attributes"][
                "field_accessibility"
            ],
            "field_accessibility_fr": response["fr"]["attributes"][
                "field_accessibility"
            ],
            "field_accessibility_it": response["it"]["attributes"][
                "field_accessibility"
            ],
            "field_teaser_color": response["de"]["attributes"]["field_teaser_color"],
            "recordset_ids": record_set_ids,
            "computed_teaser_image_url": response["de"]["attributes"][
                "computed_teaser_image_url"
            ],
            "computed_teaser_color": response["de"]["attributes"][
                "computed_teaser_color"
            ],
        }

    @staticmethod
    def _build_collections_object(response):
        taxonomy_values = (
            InstitutionObjectBuilder._get_taxonomy_values_from_collections_object(
                response
            )
        )
        result = {}
        for t in response["data"]:
            taxonomy_field_names = (
                InstitutionObjectBuilder._map_field_ids_to_taxonomy_values(
                    t["relationships"], taxonomy_values
                )
            )
            key = taxonomy_field_names["type"]
            if key != "":
                result[key] = {
                    "type": key,
                    "field_digital_number": t["attributes"]["field_digital_number"],
                    "field_digitized": t["attributes"]["field_digitized"],
                    "field_geo": taxonomy_field_names["geo"],
                    "field_indexed": t["attributes"]["field_indexed"],
                    "field_physical_number": t["attributes"]["field_physical_number"],
                    "field_subjects": taxonomy_field_names["subjects"],
                    "field_time": taxonomy_field_names["time"],
                }
        return result

    @staticmethod
    def _build_institution_addresses_object(responses):
        addresses = []
        for response in responses:
            address = response["attributes"]["field_address"]
            if address is not None:
                address["coordinates"] = response["attributes"][
                    "field_geographical_coordinates"
                ]["value"]
                address["main"] = response["attributes"]["field_primary_address"]
            addresses.append(address)
        return addresses

    @staticmethod
    def _build_institution_types_object(response):
        return [element["attributes"]["field_wikidata"]["uri"] for element in response]

    @staticmethod
    def _build_record_set_ids_object(response):
        return [
            record_set["attributes"]["field_memobase_id"] for record_set in response
        ]

    @staticmethod
    def _get_taxonomy_values_from_collections_object(response) -> dict:
        result = {"type": {}, "geo": {}, "subjects": {}, "time": {}}
        if "included" in response:
            for v in response["included"]:
                if v["type"] == "taxonomy_term--document_type":
                    if v["id"] not in result["type"]:
                        result["type"][v["id"]] = []
                    result["type"][v["id"]] = v["attributes"]["name"]
                elif v["type"] == "taxonomy_term--geo":
                    if v["id"] not in result["geo"]:
                        result["geo"][v["id"]] = []
                    result["geo"][v["id"]] = v["attributes"]["name"]
                elif v["type"] == "taxonomy_term--subject":
                    if v["id"] not in result["subjects"]:
                        result["subjects"][v["id"]] = []
                    result["subjects"][v["id"]] = v["attributes"]["name"]
                elif v["type"] == "taxonomy_term--time":
                    if v["id"] not in result["time"]:
                        result["time"][v["id"]] = []
                    result["time"][v["id"]] = v["attributes"]["name"]
        return result

    @staticmethod
    def _map_field_ids_to_taxonomy_values(relationships: dict, taxonomy: dict) -> dict:
        result = {"type": "", "geo": [], "subjects": [], "time": []}
        for relation_key, relation_value in relationships.items():
            if relation_value["data"]:
                if isinstance(relation_value["data"]) == list:
                    for field_value in relation_value["data"]:
                        if relation_key == "field_document_type":
                            result["type"] = taxonomy["type"][field_value["id"]]
                        elif relation_key == "field_geo":
                            result["geo"].append(taxonomy["geo"][field_value["id"]])
                        elif relation_key == "field_subjects":
                            result["subjects"].append(
                                taxonomy["subjects"][field_value["id"]]
                            )
                        elif relation_key == "field_time":
                            result["time"].append(taxonomy["time"][field_value["id"]])
                elif isinstance(relation_value["data"]) == dict:
                    if relation_key == "field_document_type":
                        result["type"] = taxonomy["type"][relation_value["data"]["id"]]
                    elif relation_key == "field_geo":
                        result["geo"].append(
                            taxonomy["geo"][relation_value["data"]["id"]]
                        )
                    elif relation_key == "field_subjects":
                        result["subjects"].append(
                            taxonomy["subjects"][relation_value["data"]["id"]]
                        )
                    elif relation_key == "field_time":
                        result["time"].append(
                            taxonomy["time"][relation_value["data"]["id"]]
                        )
        return result
