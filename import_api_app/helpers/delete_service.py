# Import API Service
# Copyright (C) 2020-2021 Memobase Project
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import os
import json
from import_api_app import helm
from import_api_app.app import app
from import_api_app.helpers.error import ImportApiError


class DeleteService:
    def __init__(self, logger, namespace, root_path):
        self.namespace = namespace
        self.root_path = root_path
        self.logger = logger

    def delete_record(self, record_id, session_id, dryrun) -> str:
        try:
            res = self.run_job(record_id, session_id, dryrun == "true", False)
            return json.dumps(res, ensure_ascii=True, indent=2)
        except Exception as ex:
            message = str(ex)
            self.logger.error(message)
            raise ImportApiError(message)

    def delete_recordset(self, recordset_id, session_id, dryrun) -> str:
        try:
            res = self.run_job(recordset_id, session_id, dryrun == "true", False)
            return json.dumps(res, ensure_ascii=True, indent=2)
        except Exception as ex:
            message = str(ex)
            self.logger.error(message)
            raise ImportApiError(message)

    def delete_institution(self, institution_id, session_id, dryrun) -> str:
        try:
            res = self.run_job(institution_id, session_id, dryrun == "true", False)
            return json.dumps(res, ensure_ascii=True, indent=2)
        except Exception as ex:
            message = str(ex)
            self.logger.error(message)
            raise ImportApiError(message)

    def delete_object_v2(self, ids, session_id, dryrun, recursive_delete):
        try:
            res = self.run_job(ids, session_id, dryrun, recursive_delete)
            success = res["returncode"] == 0
            msg = res["stdout"] if success else res["stderr"]
            statuscode = 200 if success else 500
            return (
                json.dumps({"returncode": res["returncode"], "message": msg}),
                statuscode,
            )
        except Exception as ex:
            message = str(ex)
            self.logger.error(message)
            raise ImportApiError(message)

    def run_job(
        self,
        object_id: [str],
        session_id: str,
        dryrun: bool,
        recursive_delete: bool,
    ):
        chart_name = f"dd-delete-service-{session_id}"
        res_install = self.do_helm_install(
            object_id,
            session_id,
            chart_name,
            app.config["delete-service-version"],
            dryrun,
            recursive_delete,
        )
        if res_install.returncode > 0:
            app.logger.error(
                (
                    "Error when installing helm chart {} in version {}"
                    + " from {}: returncode: {}; stdout: {}; stderr: {}"
                ).format(
                    chart_name,
                    app.config["delete-service-version"],
                    app.config["delete-service-chart-uri"],
                    res_install.returncode,
                    res_install.stdout,
                    res_install.stderr,
                )
            )
            self.logger.error(
                (
                    "Error when installing helm chart {}: "
                    + "returncode: {}; stdout: {}; stderr: {}"
                ).format(
                    chart_name,
                    res_install.returncode,
                    res_install.stdout,
                    res_install.stderr,
                )
            )
        retJson = {
            "status": "SUCCESS" if res_install.returncode == 0 else "FAILURE",
            "returncode": res_install.returncode,
            "stdout": res_install.stdout.rstrip().replace("\n", " -- "),
            "stderr": res_install.stderr.rstrip().replace("\n", " -- "),
        }
        return retJson

    def do_helm_install(
        self,
        object_ids: [str],
        session_id: str,
        chart_name: str,
        chart_version: str,
        dryrun: bool,
        recursive_delete: bool,
    ):
        self.logger.info(
            "calling delete service: ids={} / session={}".format(
                ",".join(object_ids), session_id
            )
        )
        job_args = {
            "ids": ";".join(object_ids),
            "dryRun": "1" if dryrun else "0",
            "recursiveDelete": "1" if recursive_delete else "0",
            "sessionId": session_id,
        }
        self.logger.info(
            "Overwrite the following settings in helm chart: --set {}".format(
                ",".join(["{}={}".format(k, v) for k, v in job_args.items()])
            )
        )
        return helm.install(
            chart_uri=app.config["delete-service-chart-uri"],
            name=chart_name,
            version=chart_version,
            namespace=self.namespace,
            set_values=job_args,
            fail_on_err=False,
        )
