import hashlib
import json
from typing import Dict, Tuple
import subprocess
import uuid

from import_api_app.helpers.configuration import ConfigurationFileHandler


def start_import_client(
    request: dict,
    record_set_id: str,
    app_name: str,
    configuration_file_handler: ConfigurationFileHandler,
) -> Tuple[Dict[str, str], int]:
    if request["job-parameters"] is None:
        return {
            "status": "FAILURE",
            "message": "Define all job parameters to start this job!",
        }, 500
    session_id = (
        request["job-parameters"]["sessionId"]
        if "sessionId" in request["job-parameters"]
        else str(uuid.uuid4())
    )
    is_not_published = (
        request["job-parameters"]["isNotPublished"]
        if "isNotPublished" in request["job-parameters"]
        else False
    )
    max_documents = (
        str(request["job-parameters"]["maxDocuments"])
        if "maxDocuments" in request["job-parameters"]
        else None
    )

    # send mapping files to result:
    conversion_file_result = configuration_file_handler.process_mapping_files(
        record_set_id
    )
    if conversion_file_result["status"] == "FATAL":
        return {
            "status": "FATAL",
            "message": str(conversion_file_result["message"]),
        }, 500

    short_session_id = hashlib.sha1(session_id.encode("UTF-8")).hexdigest()[:10]

    args = [
        "kubectl",
        "create",
        "job",
        "{}-{}".format(app_name, short_session_id),
        "--from",
        "cronjob/{}".format(app_name),
        "--dry-run=client",
        "--output",
        "json",
    ]
    res = subprocess.run(args, capture_output=True)
    if res.returncode > 0:
        return {
            "sessionId": session_id,
            "status": "FAILURE",
            "message": "Deployment failed: {}".format(res.stderr.decode()),
        }, 500

    manifest = json.loads(res.stdout.decode())
    app_args = [
        "--session-id",
        session_id,
        record_set_id,
    ]
    if max_documents:
        app_args.insert(0, "--max-documents")
        app_args.insert(1, max_documents)
    if is_not_published:
        app_args.insert(0, "--not-published")
    manifest["spec"]["template"]["spec"]["containers"][0]["args"] = app_args
    new_manifest = json.dumps(manifest)
    args = ["kubectl", "create", "-f", "-"]
    res = subprocess.run(args, input=new_manifest.encode(), capture_output=True)

    if res.returncode > 0:
        return {
            "sessionId": session_id,
            "status": "FAILURE",
            "message": "Deployment failed: {}".format(res.stderr.decode()),
        }, 500
    else:
        return {
            "sessionId": session_id,
            "status": "SUCCESS",
            "message": "Deployment successful",
        }, 201
