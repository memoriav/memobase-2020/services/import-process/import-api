# Import API Service
# Copyright (C) 2020-2021 Memobase Project
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import socket
import traceback
from os.path import join
from typing import Dict, Union

import paramiko
from kafka import KafkaProducer
from kafka.errors import KafkaTimeoutError
from paramiko.ssh_exception import (
    AuthenticationException,
    BadHostKeyException,
    SSHException,
)


class ConfigurationFileHandler:
    def __init__(
        self,
        host: str,
        port: int,
        user: str,
        password: str,
        base_path: str,
        kafka_broker_url: str,
        topic: str,
        kafka_ssl_ca_file: str,
        kafka_ssl_cert_file: str,
        kafka_ssl_key_file: str,
        logger,
    ):
        self.host = host
        self.port = port
        self.user = user
        self.password = password
        self.base_path = base_path
        self.logger = logger

        self.config_folder = "config"
        # These values are fixed and cannot be changed without updating the library!
        # https://gitlab.switch.ch/memoriav/memobase-2020/libraries/memobase-kafka-utils
        self.key_values_mapping = [
            "mapping",
            "transform",
            "localTransform",
            "pythonTransform",
            "pythonTransformConfig",
        ]
        # These file names are fixed and is what is found on the SFTP server.all
        # If you want to change them all the files on the SFTP server need to be renamed!
        # (so don't).
        self.file_name_values = [
            "mapping.yml",
            "transform.xslt",
            "localTransforms.yml",
            "json_transform.py",
            "json_transform_configs.txt",
        ]
        # these values are only used locally.
        self.status_key_names = [
            "mapping_file",
            "xslt_file",
            "local_transform_file",
            "python_file",
            "python_config_file",
        ]
        self.optional = [False, True, True, True, True]
        self.ssh_client = paramiko.SSHClient()
        self.ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        self.producer = KafkaProducer(
            bootstrap_servers=kafka_broker_url,
            key_serializer=str.encode,
            security_protocol="SSL",
            ssl_check_hostname=True,
            ssl_cafile=kafka_ssl_ca_file,
            ssl_certfile=kafka_ssl_cert_file,
            ssl_keyfile=kafka_ssl_key_file,
        )
        self.topic = topic

    def process_mapping_files(
        self, record_set_id: str
    ) -> Dict[str, Union[str, bool, list]]:
        self.logger.info(f"Retrieving mapping files for record set {record_set_id}.")
        result = self._retrieve_mapping_files(record_set_id)
        if result["status"] == "FATAL":
            self.logger.error(
                f"Could not retrieve mapping files for record set {record_set_id}."
            )
            for i in self.status_key_names:
                result[i] = False
            return result

        status = dict()
        status["messages"] = list()
        self.logger.info(
            f"Sending mapping files to kafka for record set {record_set_id}."
        )
        self.logger.info(f"Result {result}.")
        for position in range(len(self.key_values_mapping)):
            if self.key_values_mapping[position] in result:
                self.logger.info(f"Key {self.key_values_mapping[position]}")
                kafka_response = self._send_config_to_kafka(
                    record_set_id,
                    self.topic,
                    self.key_values_mapping[position],
                    result[self.key_values_mapping[position]],
                )
                if kafka_response["status"] == "SUCCESS":
                    self.logger.info(
                        f"Successfully pushed {self.key_values_mapping[position]}"
                        f" to kafka for record set {record_set_id}."
                    )
                    status[self.status_key_names[position]] = True
                else:
                    self.logger.error(
                        f"Failed to pushed {self.key_values_mapping[position]}"
                        f" to kafka for record set {record_set_id}"
                        f" because {kafka_response['message']}."
                    )
                    status[self.status_key_names[position]] = False
                    status["status"] = (
                        "FATAL" if not self.optional[position] else "WARNING"
                    )
                    status["messages"].append(kafka_response["message"])
                if "status" in status and status == "FATAL":
                    # Quickly fail if there is a fatal error!
                    return status
        if "status" not in status:
            status["status"] = "SUCCESS"
        return status

    def _retrieve_mapping_files(
        self, record_set_id: str
    ) -> Dict[str, Union[str, bool, list]]:
        try:
            self.ssh_client.connect(
                hostname=self.host,
                port=self.port,
                username=self.user,
                password=self.password,
            )
        except BadHostKeyException as ex:
            message = f"Could not connect to the server because of a bad host key: {ex}"
            self.logger.error(message)
            return {
                "status": "FATAL",
                "type": "BadHostKeyException",
                "messages": [message],
            }
        except AuthenticationException:
            message = "Could not authenticate with the sftp server with the given credentials."
            self.logger.error(message)
            return {
                "status": "FATAL",
                "type": "AuthenticationException",
                "messages": [message],
            }
        except SSHException as ex:
            message = f"SSH Exception: {ex}."
            self.logger.error(message)
            return {"status": "FATAL", "type": "SSHException", "messages": [message]}
        except socket.error as ex:
            message = f"Socket Error: {ex}."
            self.logger.error(message)
            return {"status": "FATAL", "type": "SocketError", "messages": [message]}

        sftp = self.ssh_client.open_sftp()
        path = join(self.base_path, record_set_id, self.config_folder)

        files = dict()
        for position in range(len(self.key_values_mapping)):
            file_path = join(path, self.file_name_values[position])
            self.logger.info(f"Try to load content from file {file_path}.")
            file_contents = b""
            try:
                with sftp.open(file_path, "rb") as sftpfp:
                    file_contents = sftpfp.read()
                    files[self.key_values_mapping[position]] = file_contents
                    self.logger.info(f"Loaded content from file {file_path}.")
            except IOError as err:
                if not self.optional[position]:
                    message = (
                        "Could not open the configuration file at "
                        + f"{file_path} (ERROR: {err})."
                    )
                    sftp.close()
                    self.ssh_client.close()
                    self.logger.error(message)
                    return {
                        "status": "FATAL",
                        "type": "RequiredConfigurationFileMissing",
                        "message": message,
                    }
                else:
                    self.logger.warning(
                        f"Could not open the configuration file at {file_path}. "
                        f"This file is optional and this error can be ignored if it is"
                        f" not needed. (ERROR: {err})."
                    )
                    files[self.key_values_mapping[position]] = file_contents
        sftp.close()
        self.ssh_client.close()
        files["status"] = "SUCCESS"
        self.logger.info(
            f"Successfully loaded all files for record set {record_set_id}."
        )
        return files

    def _send_config_to_kafka(
        self, record_set_id, topic: str, config_type: str, content: bytes
    ) -> Dict[str, Union[str, list]]:
        key = f"{record_set_id}#{config_type}"
        try:
            # TODO: This could be improved to actually check if the returned future succeeds.
            # However this was never a problem so far.
            self.producer.send(topic, key=key, value=content)
            return {"status": "SUCCESS"}
        except KafkaTimeoutError:
            return {
                "status": "FATAL",
                "type": "KafkaTimeOut",
                "message": f"Kafka Error: {traceback.format_exc()}.",
            }
        except AssertionError:
            return {
                "status": "FATAL",
                "type": "AssertionError",
                "message": f"Kafka Error: {traceback.format_exc()}.",
            }
